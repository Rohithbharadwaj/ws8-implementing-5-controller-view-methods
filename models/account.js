/** 
*  Account model
*  Describes the characteristics of each attribute in a account resource.
*
* @author Rajender Ravi Varma Devulapally <s534626@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const AccountSchema = new mongoose.Schema({
    acc_no: { type: Number, required: true },
    name: { type: String, required: true, default: 'Name' },
    family: { type: String, required: true, default: 'FamilyName' },
    email: { type: String, required: true, unique: true },
    no_of_tran: { type: Number, required: true } ,
    acc_balance: { type: Number, required: true } ,
    phone: { type: Number, required: true } ,
    street1: { type: String, required: true, default: '1115N College Drive' },
    street2: { type: String, required: false, default: '' },
    city: { type: String, required: true, default: 'Maryville' },
    state: { type: String, required: true, default: 'MO' },
    zip: { type: String, required: true, default: '64468' },
    country: { type: String, required: true, default: 'USA' }
  })
  
  module.exports = mongoose.model('account', AccountSchema)
  